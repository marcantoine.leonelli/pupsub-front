import React, { useState } from "react";
import styles from "./InputMessage.module.css";
import useSocket from "../../hooks/useSocket";

export const InputMessage: React.FC = () => {
  const [inputValue, setInputValue] = useState("");
  const { sendMessage } = useSocket();

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    console.log("Sending message:", inputValue);
    sendMessage(inputValue);
    setInputValue("");
  }

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    setInputValue(event.target.value);
  }

  return (
    <div className="p-4 sticky bottom-0 bg-gray-800">
      <form onSubmit={handleSubmit} className={`${styles.form} rounded-lg placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500 flex border-gray-600 bg-gray-700 align-center`}>
        <input
          type="text"
          value={inputValue}
          onChange={handleInputChange}
          className="text-white text-sm rounded-lg  p-2.5 flex-1 bg-gray-700 border-gray-600 outline-none"
        />
        <button type="submit" className="flex align-center rounded-full p-2 hover:bg-white/10">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="24"
            viewBox="0 -960 960 960"
            width="24"
            fill="#fff"
          >
            <path d="M120-160v-640l760 320-760 320Zm80-120 474-200-474-200v140l240 60-240 60v140Zm0 0v-400 400Z" />
          </svg>
        </button>
      </form>
    </div>
  );
};
