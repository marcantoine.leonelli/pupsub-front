import useSocket from '../../hooks/useSocket'
import { Message } from '../Message/Message';

export default function MessageList() {
  const { messages } = useSocket();

  return (
    <div className='flex flex-col'>
      {messages.map((message, index) => (
        <Message key={index} message={message} />
      ))}
    </div>
  )
}
