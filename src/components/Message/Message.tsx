import React from 'react'
import { Message as MessageType } from '../../../../types/message.model'

export const Message: React.FC<{message: MessageType}> = ({message}) => {
  return (
    <div className='even:bg-white/10 p-4'>
      <p className='font-bold'>{message.sender.name}</p>
      <div>{message.content}</div>
    </div>
  )
}
