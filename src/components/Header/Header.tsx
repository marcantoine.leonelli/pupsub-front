import { InputUsername } from "../InputUsername/InputUsername";

export default function Header() {
  return (
    <header className="p-4 bg-gray-900 flex justify-between items-center">
      <p>Marc-Antoine Leonelli pupsub app</p>
      <InputUsername />
    </header>
  );
}
