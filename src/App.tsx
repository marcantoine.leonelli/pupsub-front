import { useEffect } from "react";
import { socket } from "./utils/socket";
import Header from "./components/Header/Header";
import { InputMessage } from "./components/InputMessage/InputMessage";
import MessageList from "./components/MessageList/MessageList";

function App() {

  useEffect(() => {
    function onConnect() {
      console.log("Connected");
    }

    function onDisconnect() {
      console.log("Disconnected");
    }

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
    };
  }, []);

  return (
    <div className="bg-gray-800 h-full flex flex-col">
      <Header />
      <main className="flex flex-col flex-1">
        <div className="flex-1">
          <MessageList />
        </div>
        <InputMessage />
      </main>
    </div>
  );
}

export default App;
