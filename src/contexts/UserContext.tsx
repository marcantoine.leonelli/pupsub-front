import React, { createContext, useState, useEffect } from 'react';
import { User } from "../../../types/user.model";

export const UserContext = createContext<{
  user: User | null;
  updateUser: (name: string) => void;
}>(null);

export const UserProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      setUser(JSON.parse(user));
    } else {
      const newUser: User = {
        id: generateId(),
        name: 'Anonymous',
      };
      console.log(newUser);
      setUser(newUser);
    }
  }, []);

  function generateId() {
    return new Date().getTime().toString() + Math.random().toString(36);
  }

  function updateUser(name: string) {
    const newUser: User = {
      id: user.id,
      name,
    };
    setUser(newUser);
    localStorage.setItem('user', JSON.stringify(newUser));
  }

  if (!user) {
    return null;
  }

  return (
    <UserContext.Provider value={{ user, updateUser }}>
      {children}
    </UserContext.Provider>
  );
};
