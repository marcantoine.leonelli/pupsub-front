import { useState, useEffect, createContext } from "react";
import { socket } from "../utils/socket";
import useUser from "../hooks/useUser";
import { Message } from "../../../types/message.model";

export const MessageContext = createContext<{
  messages: Message[];
  sendMessage: (content: string) => void;
}>(null);

export function MessageProvider({ children }) {
  const { user } = useUser();
  const [messages, setMessages] = useState<Message[]>([]);

  useEffect(() => {
    function onMessageEvent(value: Message) {
      console.log("Message:", value);
      setMessages((previous) => [...previous, value]);
    }

    socket.on("message", onMessageEvent);

    return () => {
      socket.off("message", onMessageEvent);
    };
  }, []);

  function sendMessage(content: string) {
    socket.emit('message', {
      content,
      sender: user,
    });
  }

  return (
    <MessageContext.Provider value={{ messages, sendMessage }}>
      {children}
    </MessageContext.Provider>
  );
}
