import { useContext } from "react";
import { MessageContext } from "../contexts/MessageContext";

export default function useSocket() {
  return useContext(MessageContext);
}