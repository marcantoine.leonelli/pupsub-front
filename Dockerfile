FROM node:18-alpine

WORKDIR /app/front

COPY ./front/package*.json .

RUN npm ci

COPY ./front/ .
COPY ../types/ ../types/

RUN npm run build

EXPOSE 8080

CMD [ "npm", "run", "serve" ]